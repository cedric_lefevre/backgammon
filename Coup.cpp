#include <iostream>
#include "Joueur.cpp"
#include "Des.cpp"
using namespace std;

class Coup
{
public:
	Coup();
	~Coup();
	bool pos(int ca, Dame p, Des d) const;
	void joue(int ca, Dame p, Des d);
private:
	Des un, deux;
	Joueur j;	
};