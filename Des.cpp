#include <iostream>

using namespace std;

class Des
{
public:
	Des();
	~Des();
	int val() const;
	void jet();
	void aff();
    bool operator==(Des const& a);
private:
	int valeur;	
};


inline int Des::val() const{
	return valeur;
}

inline void Des::jet(){
	valeur=rand()%6+1;
}


inline bool Des::operator==(Des const& a){
	if(valeur==a.val())
		return true;
	else
		return false;
}


inline void Des::aff()
{
    switch(val()){
    	case 1: 	
    		cout<<" - - - "<<endl<<
    		      "|     |"<<endl<<
    		      "|  °  |"<<endl<<
    		      "|     |"<<endl<<
    		      " - - - "<<endl;
    		break;
    	case 2: 	
    		cout<<" - - - "<<endl<<
    		     "| °   |"<<endl<<
    		     "|     |"<<endl<<
    		     "|   ° |"<<endl<<
    		     " - - - "<<endl;
    		break;
    	case 3: 	
    		cout<<" - - - "<<endl<<
    		     "| °   |"<<endl<<
    		     "|  °  |"<<endl<<
    		     "|   ° |"<<endl<<
    		     " - - - "<<endl;
    		break;
    	case 4: 	
    		cout<<" - - - "<<endl<<
    		     "|°   °|"<<endl<<
    		     "|     |"<<endl<<
    		     "|°   °|"<<endl<<
    		     " - - - "<<endl;
    		break;
    	case 5: 	
    		cout<<" - - - "<<endl<<
    		     "|°   °|"<<endl<<
    		     "|  °  |"<<endl<<
    		     "|°   °|"<<endl<<
    		     " - - - "<<endl;
    		break;
    	case 6: 	
    		cout<<" - - - "<<endl<<
    		     "|°   °|"<<endl<<
    		     "|°   °|"<<endl<<
    		     "|°   °|"<<endl<<
    		     " - - - "<<endl;
    		break;
    }
}