#include <iostream>
#include "Plateau.cpp"
#include "Coup.cpp"
using namespace std;

class Partie
{
public:
	Partie();
	~Partie();
private: 
	Des d1, d2;
	Joueur j1, j2;
	Plateau plat;
	stack<Coup> coups;
};

inline Partie::Partie(){
	plat.aff();
}