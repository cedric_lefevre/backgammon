#include <iostream>
using namespace std;

class Dame
{
public:
	Dame(char c);
	~Dame();
	char val() const;
	bool operator==(Dame const& a);
private:
	char coul;
};


inline Dame::Dame(char c){
	coul=c;
}

inline char Dame::val() const{
	return coul;
}


inline bool Dame::operator==(Dame const& a){
	if(coul==a.val())
		return true;
	else
		return false;
}