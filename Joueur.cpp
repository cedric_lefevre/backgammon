#include <iostream>
#include <string>
using namespace std;

class Joueur
{
public:
	Joueur();
	~Joueur();
	void init(string n);
	void init(int stat);
private:
	string nom;
	int vict;
	int strat;
};

inline void Joueur::init(string n){
	nom=n;
	vict=0;
	strat=0;
}

inline void Joueur::init(int s){
	vict=0;
	strat=s;
}