#include <iostream>
#include "Dame.cpp"
#include <stack>
using namespace std;

class Case
{
public:
	Case();
	~Case();
	void init(int nbDames, char coul);
	int size();
	char coul();
	void add(Dame d);
	void pop();
private:
	stack<Dame> set;	
};


inline void Case::init (int nb, char coul){
	for(int i=0; i<nb; ++i)
		set.push(Dame(coul));
}

inline int Case::size(){
	return set.size();
}

inline char Case::coul(){
	if(set.empty())
		return 'a';
	else
		return set.top().val();
}


inline void Case::add(Dame d){
	set.push(d);
}


inline void Case::pop(){
	set.pop();
}